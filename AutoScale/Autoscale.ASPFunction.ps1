
$sub = "xxxxxxxx-xxxx...."
$rg = "one"
$appServicePlanName = "one"

$appServicePlanResourceId = "/subscriptions/"+ $sub + "/resourceGroups/" + $rg + "/providers/Microsoft.Web/serverfarms/" + $appServicePlanName

#-MetricResourceId $websiteResourceId
$Rule = New-AzureRmAutoscaleRule -MetricName "CpuPercentage" -MetricResourceId $appServicePlanResourceId -Operator GreaterThan -MetricStatistic Average -Threshold 10 -TimeGrain 00:01:00 -ScaleActionCooldown 00:05:00 -ScaleActionDirection Increase -ScaleActionScaleType ChangeCount -ScaleActionValue "1"

$Autoscaleprofile = New-AzureRmAutoscaleProfile -Name "MusicCaseAutoScale1" -DefaultCapacity "1" -MaximumCapacity "3" -MinimumCapacity "1" -Rule $Rule 

Add-AzureRmAutoscaleSetting -Location "Central US" -Name "MCAutoScale1" -ResourceGroupName $rg -TargetResourceId $appServicePlanResourceId -AutoscaleProfiles $Autoscaleprofile
