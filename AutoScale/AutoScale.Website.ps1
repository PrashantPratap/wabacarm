
$sub = "xxxxxxxx-xxxx..."
$rg = "wabac"
$websiteName = "wabac"
$appServicePlanName = "wabacasp"

$websiteResourceId = "/subscriptions/" + $sub + "/resourceGroups/" + $rg + "/providers/Microsoft.Web/sites/" + $websiteName
$appServicePlanResourceId = "/subscriptions/"+ $sub + "/resourceGroups/" + $rg + "/providers/Microsoft.Web/serverfarms/" + $appServicePlanName

$Rule = New-AzureRmAutoscaleRule -MetricName "CpuTime" -MetricResourceId $websiteResourceId -Operator GreaterThan -MetricStatistic Average -Threshold 10 -TimeGrain 00:01:00 -ScaleActionCooldown 00:05:00 -ScaleActionDirection Increase -ScaleActionScaleType ChangeCount -ScaleActionValue "1"

$Autoscaleprofile = New-AzureRmAutoscaleProfile -Name "MusicCaseAutoScale" -DefaultCapacity "1" -MaximumCapacity "3" -MinimumCapacity "1" -Rule $Rule 

Add-AzureRmAutoscaleSetting -Location "Central US" -Name "MCAutoScale" -ResourceGroupName $rg -TargetResourceId $appServicePlanResourceId -AutoscaleProfiles $Autoscaleprofile
