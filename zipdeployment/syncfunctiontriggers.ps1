# update resource group name and sitename
# run these cmdlets 

# Login-AzureRmAccount
# .\syncfunctiontriggers.ps1

# for more details : https://github.com/davidebbo/AzureWebsitesSamples/blob/master/PowerShell/HelperFunctions.ps1#L350-L355

$ResourceGroupName = "CASES.TOMTOM"
$SiteName = "casestomtom-cdkywahtyl7ly"
$WebAppApiVersion = "2015-08-01"

Function SyncFunctionAppTriggers($ResourceGroupName, $SiteName, $Slot)
{
    $ResourceType,$ResourceName = GetResourceTypeAndName $SiteName $Slot
   Invoke-AzureRmResourceAction -ResourceGroupName $ResourceGroupName -ResourceType $ResourceType -ResourceName $SiteName -Action syncfunctiontriggers -ApiVersion $WebAppApiVersion -Force
}

Function GetResourceTypeAndName($SiteName, $Slot)
{
    $ResourceType = "Microsoft.Web/sites"
    $ResourceName = $SiteName

    if ($Slot) {
        $ResourceType = "$($ResourceType)/slots"
        $ResourceName = "$($ResourceName)/$($Slot)"
    }
    $ResourceType,$ResourceName
}

SyncFunctionAppTriggers $ResourceGroupName $SiteName